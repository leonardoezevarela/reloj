import React from "react";
import Button from 'react-bootstrap/Button'
import Spinner from 'react-bootstrap/Spinner'

const styles={
    spinner:{
        marginRight:"5px"
    }
}
function ButtonWithLoading(props){
    return(
        <Button variant="primary" type={props.type?props.type:"submit"}>
            {
                props.loading &&
                <Spinner animation="border" variant="light" size="sm" style={styles.spinner} />
            }
            {props.text || "Ingresar"}
        </Button>
    )
}

export default ButtonWithLoading
