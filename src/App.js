import React, { Component } from "react";
import Home from "./Home"
import Registro from "./Registro"
import Login from "./Login"
import Menu from "./Menu";
import Producto from "./Producto"
import {BrowserRouter,Route} from "react-router-dom";
import firebase from './Config/firebase';

class App extends Component{

    constructor(){
        super();
        this.state={
            opcionesMenu:[
                {
                    path:"/registro",
                    label:"Registro"
                },
                {
                    path:"/login",
                    label:"Login"
                },
                {
                    path:"/",
                    label:"Home"
                }
            ]
        }
        console.log(firebase.database())
    }
    
    render() {
        return (
            <div>
                <div>
                    
               


                    <BrowserRouter>
                        <Menu options={this.state.opcionesMenu} title={this.state.titulo} click={this.handleClick}/>
                        <Route path={"/"} component={Home} exact />
                        <Route path={"/registro"} component={Registro} exact /> 
                        <Route path={"/login"} component={Login} exact />
                        <Route path={"/producto/:id"} component={Producto} exact /> 

                    </BrowserRouter>
                  
                    
                </div>
            </div>
        )
    }
}

export default App;