import React from "react";
import Spinner from 'react-bootstrap/Spinner'

const styles={
    container:{
        position:"fixed",
        top:"50%",
        left:"50%"
    }
}

function SpinnerGeneral(){
    return(
        <div style={styles.container}>
            <Spinner animation="grow" variant="primary" />
            <Spinner animation="grow" variant="primary" />
            <Spinner animation="grow" variant="primary" />
        </div>
    )
}

export default SpinnerGeneral
