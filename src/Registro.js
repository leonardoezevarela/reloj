import React, {useState} from "react";
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import firebase from "./Config/firebase"
import { useHistory } from "react-router-dom"
import ButtonWithLoading from "./ButtonWithLoading";

const styles = {
    cards:{
        width:'70%',
        margin:'auto',
        marginTop:'40px'

    }
}

function Registro(){
    const history = useHistory();
    const [form,setForm] = useState({nombre:'', apellido: '',email: '',password:''});
    const [spinner,setSpinner] = useState(false);
    const handleSubmit = (e)=>{
        console.log("HandleSubmit",form)
        setSpinner(true);
        firebase.auth.createUserWithEmailAndPassword(form.email,form.password)
        .then(data=>{
            console.log("data",data.user.uid)
            firebase.db.collection("usuarios").add({
                nombre:form.nombre,
                apellido:form.apellido,
                email:form.email,
                userId:data.user.uid
            })
            .then(data=>{
                console.log("data database",data)
                setSpinner(false);
                history.push("/login")
            })
            .catch(error=>{
                setSpinner(false);
                console.log("error database",error)
            })
        })
        .catch(err=>{
            console.log("error",err)
            setSpinner(false);
        })
        e.preventDefault();
    }
    const handleChange = (e)=>{
        const target = e.target;
        const value = target.value;
        const name = target.name;

        setForm({
            ...form,
            [name]:value
        })
    }


    return(
        <>
<Card style={styles.cards}>
  <Card.Body>
    <Card.Title>Registrarse</Card.Title>
    <Form onSubmit={handleSubmit}>
        <Form.Group controlId="formBasicNombre">
            <Form.Label>Nombre</Form.Label>
            <Form.Control type="text" placeholder="Ingrese su nombre" name="nombre" value={form.nombre} onChange={handleChange}/>
        </Form.Group>

        <Form.Group controlId="formBasicApellido">
            <Form.Label>Apellido</Form.Label>
            <Form.Control type="text" placeholder="Ingrese su apellido" name="apellido" value={form.apellido} onChange={handleChange}/>
        </Form.Group>

        <Form.Group controlId="formBasicEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Ingrese su email" name="email" value={form.email} onChange={handleChange}/>
        </Form.Group>

        <Form.Group controlId="formBasicPassword">
            <Form.Label>Contraseña</Form.Label>
            <Form.Control type="password" placeholder="Ingrese su contraseña" name="password" value={form.password} onChange={handleChange}/>
        </Form.Group>

        <ButtonWithLoading text="Registrarse" loading={spinner}/>

    </Form>

  </Card.Body>
</Card>

        </>
    )
}
export default Registro