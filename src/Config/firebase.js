import firebase from "firebase";

import "firebase/auth";
import "firebase/firestore";

// Initialize Firebase
const firebaseConfig = {
        apiKey: "AIzaSyCYY1x3q2Nk9Ne_GaNmofCGZGDgmcUA7E8",
        authDomain: "react-ff4b0.firebaseapp.com",
        databaseURL: "https://react-ff4b0.firebaseio.com",
        projectId: "react-ff4b0",
        storageBucket: "react-ff4b0.appspot.com",
        messagingSenderId: "209930906117",
        appId: "1:209930906117:web:e45c2d0a29d2dfa33cca6c"
};
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();
db.settings({
        timestampsInSnapshot: true
});
firebase.auth=firebase.auth();
firebase.db=db;

export default firebase;