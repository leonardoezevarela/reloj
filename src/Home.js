import React, { Component } from "react";
import ProductoDetalle from "./ProductoDetalle";
import firebase from "./Config/firebase";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import SpinnerGeneral from "./SpinnerGeneral"


class Home extends Component{
    constructor(){
        super()
        this.state={
            productos:[],
            loading:true
        }
    }
    componentDidMount() {
        console.log("componentDidMount")
       firebase.db.collection("productos")
       .get()
       .then((querySnapshot)=>{

           this.setState({
               productos: querySnapshot.docs,
               loading: false,
           });
       });
    }

    render(){
        if(this.state.loading){
            return(
                <SpinnerGeneral />
            )
        }else{
            return (
                <div>
                    <Container>
                    <Row xs={2} md={3} lg={4} sg={5}>
                       {this.state.productos.map((producto) => (<ProductoDetalle id={producto.id} data={producto.data()} buttons={true}/>))}
                    </Row>
                    </Container>

                </div>
            )
        } 
    }
}
export default Home;